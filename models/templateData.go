package models

import "github.com/quadrosh/dinacleango/forms"

// TemplateData is data to template
type TemplateData struct {
	StringMap map[string]string
	IntMap    map[string]int
	FloatMap  map[string]float32
	Data      map[string]interface{}
	CSRFToken string
	Flash     string
	Warning   string
	Error     string
	IntSlice  []int
	// FuncMap   map[string]interface{}
	Form  *forms.Form
	Order Order
	Modal string
}
