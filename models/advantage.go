package models

import "html/template"

// Advantage is model of advantage section item
type Advantage struct {
	ID          int
	Name        string
	Description template.HTML
}
