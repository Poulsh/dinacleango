package models

import "time"

// Order ...
type Order struct {
	ID          int
	WorkDate    string
	WorkType    string
	Workplace   string
	Area        string
	Address     string
	Name        string
	Phone       string
	Comment     string
	Contacts    string
	Email       string
	Date        time.Time
	Status      string
	UtmContent  string
	UtmSource   string
	UtmMedium   string
	UtmCampaign string
	UtmTerm     string
}
