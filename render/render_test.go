package render

import (
	"net/http"
	"testing"

	"github.com/quadrosh/dinacleango/models"
)

func TestAddDefaultData(t *testing.T) {

	var td models.TemplateData

	r, err := getSession()
	if err != nil {
		t.Error(err)
	}

	session.Put(r.Context(), "flash", "123")
	result := AddDefaultData(&td, r)
	if result.Flash != "123" {
		t.Error("AddDefaultData() failed: flash value of 123 not found in session ", err)
	}

}

func TestRenderTemplate(t *testing.T) {
	pathToTemplates = "./../templates"
	tc, err := CreateTemplateCache()
	if err != nil {
		t.Error(err)
	}
	app.TemplateCache = tc
	r, err := getSession()
	if err != nil {
		t.Error(err)
	}

	var ww myWriter
	err = RenderTemplate(&ww, r, "home.page.tmpl", &models.TemplateData{})
	if err != nil {
		t.Error("Error writing template to browser: ", err)
	}

	err = RenderTemplate(&ww, r, "not-existent.page.tmpl", &models.TemplateData{})
	if err == nil {
		t.Error("Not found Error writing not existent template to browser: ", err)
	}
}

func getSession() (*http.Request, error) {
	r, err := http.NewRequest("GET", "/some-url", nil)
	if err != nil {
		return nil, err
	}

	ctx := r.Context()
	ctx, _ = session.Load(ctx, r.Header.Get("X-Session"))
	r = r.WithContext(ctx)

	return r, err
}

// its just sets the config and not returns anything
func TestNewRenderer(t *testing.T) {
	NewRenderer(app)
}

func TestCreateTemplateCache(t *testing.T) {
	pathToTemplates = "./../templates"

	_, err := CreateTemplateCache()
	if err != nil {
		t.Error("CreateTemplateCache error: ", err)
	}
}
