$(document).ready(function() {


    //    gsap  range  slider   https://codepen.io/breheny/pen/zxMWxd



    function int_000(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }



    if (document.getElementById('homeSlickTopCarusel')) {

        $('.slickTopCarusel').slick( 
            {
                enabled: true,
                autoplay: false,
                draggable: false,
                autoplaySpeed: 10000,
                easing:'easeInOutSine',
                prevArrow:'.slickTopCarusel_slickPrev',
                nextArrow:'.slickTopCarusel_slickNext',
                method: {},
        
            }
            
        );

        $('.slickTopCarusel').on('beforeChange', function(event, { slideCount: count }, currentSlide, nextSlide){
            var slideOut = new TimelineMax()
            .set(".caru_lead", {css:{autoAlpha:0}, ease:Power1.easeIn})
            .set("#callMeWrap", {css:{autoAlpha:0,rotationY:270,transformOrigin:"50% 100% 100"}})
            .set(".caru_lead2", {css:{autoAlpha:0,rotationX:90, y:0, transformOrigin:"50% 100%"}})
            .set(".caru_bgr_circle", {css:{autoAlpha:0}, ease:Power1.easeIn})
            ;
            // let selectors = [nextSlide, nextSlide - count, nextSlide + count].map(n => `[data-slick-index="${n}"]`).join(', ');
            // $('.slick-now').removeClass('slick-now');
            // $(selectors).addClass('slick-now');
        });

        $('.slickTopCarusel').on('afterChange', function(event, { slideCount: count }, currentSlide, nextSlide){
            var slideIn = new TimelineMax()
            .fromTo(".caru_bgr_circle", 2,
                {css:{autoAlpha:0, scale:0.7, transformOrigin:"50% 50% "}},
                {css:{autoAlpha:1, scale:1.0}, ease: Elastic.easeOut.config(1, 0.3)}
                , "circleIn")
            .fromTo(".caru_lead", 1,
                {css:{autoAlpha:0,rotationX:360, y:"-300"}, ease:Power1.easeOut},
                {css:{autoAlpha:1,rotationX:0, y:0}, ease:Power1.easeOut}
                , "circleIn+=1")
            .to(".caru_lead2", 1,
                {css:{autoAlpha:1,rotationX:0, y:0}, ease:Power3.easeOut}
                , "lead2")
            .to("#callMeWrap", 1,
                {css:{autoAlpha:1, rotationY:0, y:0}, ease:Power3.easeIn}
                , "lead2")
            .to("#callMeBtn", 0.7,
                {css:{ scaleY:1.2}, ease:Power1.easeIn}
                , "lead2+=3")
            .to("#callMeBtn", 1,
                {css:{ scaleY:1}, ease: Elastic.easeOut}
                )
            ;
        });

    }




    // if (document.getElementsByClassName('slickMulti')) {
    //     var arr = document.getElementsByClassName('slickMulti');

    //     for (var i = 0; i < arr.length; i++) {
    //         var id = arr[i].getAttribute('data-id');
    //         var showItems = arr[i].getAttribute('data-showItems')? arr[i].getAttribute('data-showItems'):1;
    //         var autoplay = arr[i].getAttribute('data-autoplay')? arr[i].getAttribute('data-autoplay'):false;
    //         slick(id,showItems,autoplay);
    //     }

    //     function slick(id,show,autoplay) {
    //         $('.slick_multi_'+id).slick({
    //             infinite: true,
    //             autoplay: autoplay,
    //             slidesToShow: show,
    //             slidesToScroll: 1,
    //             dots: false,
    //             easing:'easeInOutSine',
    //             prevArrow:'.slickPrev'+id,
    //             nextArrow:'.slickNext'+id,
    //             responsive: [
    //                 {
    //                     breakpoint: 769,
    //                     settings: {
    //                         slidesToShow: 1
    //                     }
    //                 }
    //             ]
    //         });
    //     }
    // }



    if (document.getElementById('reviewsCaruselDiv')) {
        $('.reviewsCarusel').slick( 
            {
                enabled: true,
                autoplay: false,
                draggable: true,
                autoplaySpeed: 10000,
                slidesToShow: 3,
                slidesToScroll: 1,
                easing:'easeInOutSine',
                prevArrow:'.slickReviewsCarusel_slickPrev',
                nextArrow:'.slickReviewsCarusel_slickNext',
                method: {},
        
            }
            
        );
    }
    


    
    

});





function Prompt(){
    let toast = function(c) {
      const {
        msg = '',
        icon = 'success',
        position = 'top-end',
      } = c;

      const Toast = Swal.mixin({
        toast: true,
        title: msg,
        position: position,
        icon: icon,
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({})
    }

    let success = function(c) {
      const{
        msg = '',
        title = '',
        footer = '',
      } = c;

      Swal.fire({
        icon: 'success',
        title: title,
        text: msg,
        footer: footer
      });
    }

    let error = function(c) {
      const{
        msg = '',
        title = '',
        footer = '',
      } = c;

      Swal.fire({
        icon: 'error',
        title: title,
        text: msg,
        footer: footer
      });
    }

    let custom = async function(c) {
      const{
        icon = "",
        msg = "",
        title = "",
        showConfirmButton = true,
        showCancelButton = true,
        showCloseButton = false,
        focusConfirm = false,
      } = c;
      const { value: result } = await Swal.fire({
        icon: icon,
        title: title,
        html: msg,
        backdrop: false,
        focusConfirm: false,
        showCloseButton: showCloseButton,
        showCancelButton: showCancelButton,
        showConfirmButton: showConfirmButton,
        focusConfirm: focusConfirm,
        willOpen: () => {
         if (c.willOpen !== undefined) {
           c.willOpen();
         }
        },
        preConfirm: () => {
          return [
            document.getElementById('start').value,
            document.getElementById('end').value
          ]
        },
        didOpen: () => {
          if(c.didOpen !== undefined) {
            c.didOpen();
          }
        }
      })

      if(result) {
        if(result.dismiss !== Swal.DismissReason.cancel){
          if(result.value != ""){
            if(c.callback != undefined){
              c.callback(result);
            } else {
              c.callback(false);
            }
          } else {
              c.callback(false);
            }
        }
      }
      // if (formValues) {
      //   Swal.fire(JSON.stringify(formValues))
      // }
    }

    return {
      toast:toast,
      success:success,
      error:error,
      custom:custom,
    }
  }