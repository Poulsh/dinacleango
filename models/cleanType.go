package models

// CleanType ...
type CleanType struct {
	ID          int
	Name        string
	Description string
}
