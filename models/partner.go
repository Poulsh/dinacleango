package models

// Partner ...
type Partner struct {
	ID          int
	Name        string
	Description string
	Image       string
}
