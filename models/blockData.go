package models

// BlockData is the type of data for block
type BlockData struct {
	Head                      string
	Head2                     string
	Text                      string
	Prices                    []Price
	Tasks                     []Task
	IsTypeSupporting          bool
	IsTypeGeneral             bool
	IsTypeAfterReconstruction bool
	IsTypeWindows             bool
	Button1Url                string
	Button1Name               string
	Button1Active             bool
	Button2Url                string
	Button2Name               string
	Button2Active             bool
}
