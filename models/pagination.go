package models

type Pagination struct {
	ItemFirst   int
	ItemLast    int
	Total       int
	PageSize    int
	CurrentPage int
}

func (p *Pagination) Construct() {
	if p.CurrentPage > 1 {
		p.ItemFirst = (p.PageSize * (p.CurrentPage - 1)) + 1
	} else {
		p.ItemFirst = 1
	}
	if p.Total-p.ItemFirst > p.PageSize {
		p.ItemLast = p.ItemFirst + p.PageSize - 1
	} else {
		p.ItemLast = p.ItemFirst + (p.Total - p.ItemFirst)
	}
}
