package handlers

import (
	"encoding/gob"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/justinas/nosurf"
	"github.com/quadrosh/dinacleango/config"
	"github.com/quadrosh/dinacleango/helpers"
	"github.com/quadrosh/dinacleango/mailer"
	"github.com/quadrosh/dinacleango/models"
	"github.com/quadrosh/dinacleango/render"
)

var app config.AppConfig

var session *scs.SessionManager

var pathToTemplates = "./../../"

var functions = template.FuncMap{}

func getRoutes() http.Handler {

	app.MailSMTP = "localhost"
	app.MailPort = 25
	app.MailLogin = ""
	app.MailPassword = ""
	app.MailEncrypted = false
	app.MailFrom = "from@test.ru"
	app.MailTo = "to@test.ru"

	lConfig := config.LocalConfig{
		InProduction: false,
		UseCache:     true,
		DbHost:       "localhost",
		DbName:       "dinacleaning", // dinacleaningsrv
		DbUser:       "tb303",
		DbPass:       "303",
		DbPort:       "3306", // 8889 or 3306
		AppPort:      "8080",
	}

	// types, which can be put in the session
	gob.Register(models.Page{})
	gob.Register([]models.Page{})
	gob.Register([]models.HomeSlide{})
	gob.Register([]models.Advantage{})
	gob.Register([]models.Partner{})
	gob.Register([]models.Review{})
	gob.Register(models.BlockData{})
	gob.Register([]string{})

	mailer.ListenForMail(&app) // it blocks the execution of test

	mailChan := make(chan models.MailData)
	app.MailChan = mailChan

	// change this to true when in production
	app.InProduction = lConfig.InProduction

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	app.InfoLog = infoLog

	errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	app.ErrorLog = errorLog

	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = app.InProduction

	app.Session = session

	tc, err := CreateTestTemplateCache()
	if err != nil {
		log.Fatal("cannot create template cache")
	}

	app.TemplateCache = tc
	app.UseCache = lConfig.UseCache

	repo := NewTestRepo(&app)

	render.NewRenderer(&app)
	NewHandlers(repo)

	helpers.NewHelpers(&app)

	mux := chi.NewRouter()
	mux.Use(middleware.Recoverer)
	// mux.Use(NoSurf)
	mux.Use(SessionLoad)

	mux.Get("/", Repo.Home)

	mux.Get("/favicon.ico", Repo.FaviconHandler)

	mux.Post("/windows-cleaning-json", Repo.CleanWindowsDefaultJSON)
	mux.Post("/windows-cleaning-prices-json", Repo.CleanWindowsPricesJSON)
	mux.Post("/windows-cleaning-vhodit-json", Repo.CleanWindowsVhoditJSON)

	mux.Post("/after-reconstruction-cleaning-json", Repo.CleanAfterReconstructionDefaultJSON)
	mux.Post("/after-reconstruction-cleaning-prices-json", Repo.CleanAfterReconstructionPricesJSON)
	mux.Post("/after-reconstruction-cleaning-vhodit-json", Repo.CleanAfterReconstructionVhoditJSON)

	mux.Post("/general-cleaning-json", Repo.CleanGeneralDefaultJSON)
	mux.Post("/general-cleaning-prices-json", Repo.CleanGeneralPricesJSON)
	mux.Post("/general-cleaning-vhodit-json", Repo.CleanGeneralVhoditJSON)

	mux.Post("/support-cleaning-json", Repo.CleanSupportDefaultJSON)
	mux.Post("/support-cleaning-prices-json", Repo.CleanSupportPricesJSON)
	mux.Post("/support-cleaning-vhodit-json", Repo.CleanSupportVhoditJSON)
	mux.Post("/submit-order", Repo.SubmitOrderMain)
	mux.Get("/submit-order", Repo.SubmitOrderMain)

	mux.Post("/submit-phone-order", Repo.PostSubmitPhoneForm)

	fileServer := http.FileServer(http.Dir("./assets/"))
	mux.Handle("/assets/*", http.StripPrefix("/assets", fileServer))

	mux.Get("/admin/dashboard", Repo.AdminDashboard)
	mux.Get("/admin/orders", Repo.AdminOrders)

	return mux
}

// NoSurf adds CSRF protection for all POST requests
func NoSurf(next http.Handler) http.Handler {
	csrfHandler := nosurf.New(next)
	csrfHandler.SetBaseCookie(http.Cookie{
		HttpOnly: true,
		Path:     "/",
		Secure:   app.InProduction,
		SameSite: http.SameSiteLaxMode,
	})
	return csrfHandler
}

// SessionLoad loads and sets session on every request
func SessionLoad(next http.Handler) http.Handler {
	return session.LoadAndSave(next)
}

// CreateTestTemplateCache creates template cashe
func CreateTestTemplateCache() (map[string]*template.Template, error) {
	myCache := map[string]*template.Template{}

	pages, err := filepath.Glob(fmt.Sprintf("%s/*.page.tmpl", pathToTemplates))
	if err != nil {
		log.Println("error: ", err)
		return myCache, err
	}

	for _, page := range pages {

		name := filepath.Base(page)
		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
			return myCache, err
		}

		matches, err := filepath.Glob(fmt.Sprintf("%s/*.layout.tmpl", pathToTemplates))
		if err != nil {
			return myCache, err
		}

		if len(matches) > 0 {
			ts, err = ts.ParseGlob(fmt.Sprintf("%s/*.layout.tmpl", pathToTemplates))
			if err != nil {
				return myCache, err
			}
		}

		myCache[name] = ts
	}

	return myCache, nil
}
