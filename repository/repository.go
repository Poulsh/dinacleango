package repository

import (
	"github.com/quadrosh/dinacleango/models"
)

// DatabaseRepo is interface functions to work with db
type DatabaseRepo interface {
	GetHomePages() ([]models.Page, error)
	GetHomeSlides() ([]models.HomeSlide, error)
	GetPrices(cleanType int) ([]models.Price, error)
	GetPageByID(id int) (models.Page, error)
	GetTasks(cleanType int) ([]models.Task, error)
	GetAdvantages() ([]models.Advantage, error)
	GetPartners() ([]models.Partner, error)
	GetReviews() ([]models.Review, error)
	GetCleanTypes() ([]models.CleanType, error)
	CreateOrder(i models.Order) error
	Authenticate(email, password string) (int, string, error)
	GetUserByPasswordResetToken(token string) (models.User, error)
	HashPassword(token string, password string) error
	GetOrders(models.Pagination) (models.Pagination, []models.Order, error)
}
