package models

// Price contain price item
type Price struct {
	ID         int
	ListOrder  int
	TypeID     int
	Name       string
	NameDescr  string
	Price      int
	PriceDescr string
}
