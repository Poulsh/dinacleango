package dbrepo

import (
	"context"
	"errors"
	"fmt"
	"html/template"
	"log"
	"strings"
	"time"

	"github.com/quadrosh/dinacleango/models"
	"golang.org/x/crypto/bcrypt"
)

func (m *mysqlDBRepo) GetHomePages() ([]models.Page, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var pages []models.Page

	query := `
		select 
			p.id, 
			p.hrurl,
			p.seo_insert,
			p.title,
			p.description,
			p.pagehead,
			p.pagedescription
		from
			pages p
		where 
			p.hrurl = ?
		or 
			p.hrurl = ?	
		or 
			p.hrurl = ?	
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query, "home", "partners", "review")
	if err != nil {
		return pages, err
	}

	for rows.Next() {
		var page models.Page
		err := rows.Scan(
			&page.ID,
			&page.Hrurl,
			&page.SeoInsert,
			&page.Title,
			&page.Description,
			&page.Pagehead,
			&page.Pagedescription,
		)
		if err != nil {
			return pages, err
		}
		pages = append(pages, page)
	}

	if err = rows.Err(); err != nil {
		return pages, err
	}

	return pages, nil

}

func (m mysqlDBRepo) GetHomeSlides() ([]models.HomeSlide, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var slides []models.HomeSlide
	query := `
		select 
			*
		from
			home_slides
						;		
	`
	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return slides, err
	}
	for rows.Next() {
		var slide models.HomeSlide
		err := rows.Scan(
			&slide.ID,
			&slide.Title,
			&slide.Lead,
			&slide.Lead2,
			&slide.Text,
			&slide.Image,
			&slide.Promolink,
			&slide.Promoname,
		)
		if err != nil {
			return slides, err
		}
		slides = append(slides, slide)
	}

	if err = rows.Err(); err != nil {
		return slides, err
	}

	return slides, nil
}

func (m mysqlDBRepo) GetPrices(cleanType int) ([]models.Price, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var prices []models.Price

	query := `
		select 
			p.id, 
			IFNULL(p.list_order, 0),
			p.type_id,
			p.name,
			IFNULL(p.name_descr, ''),
			IFNULL(p.price, 0),
			p.price_descr
		from
			prices p
		where 
			p.type_id = ?
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query, cleanType)
	if err != nil {
		return prices, err
	}

	for rows.Next() {
		var price models.Price
		err := rows.Scan(
			&price.ID,
			&price.ListOrder,
			&price.TypeID,
			&price.Name,
			&price.NameDescr,
			&price.Price,
			&price.PriceDescr,
		)
		if err != nil {
			return prices, err
		}
		prices = append(prices, price)
	}

	if err = rows.Err(); err != nil {
		return prices, err
	}
	return prices, nil
}

func (m mysqlDBRepo) GetPageByID(id int) (models.Page, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var page models.Page

	query := `
		select 
			p.id, 
			p.hrurl,
			p.seo_insert,
			p.title,
			p.description,
			p.pagehead,
			p.pagedescription
		from
			pages p
		where 
			p.id = ?
			;	
	`
	row := m.DB.QueryRowContext(ctx, query, id)
	err := row.Scan(
		&page.ID,
		&page.Hrurl,
		&page.SeoInsert,
		&page.Title,
		&page.Description,
		&page.Pagehead,
		&page.Pagedescription,
	)
	if err != nil {
		return page, err
	}

	return page, nil
}

func (m mysqlDBRepo) GetTasks(cleanType int) ([]models.Task, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var tasks []models.Task

	query := `
		select 
			t.id, 
			IFNULL(t.parent_id, 0),
			IFNULL(t.name, '')
		from
			tasks t
		where 
			t.parent_id = ?
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query, cleanType)
	if err != nil {
		return tasks, err
	}

	for rows.Next() {
		var task models.Task
		err := rows.Scan(
			&task.ID,
			&task.ParentID,
			&task.Name,
		)
		if err != nil {
			return tasks, err
		}
		tasks = append(tasks, task)
	}
	if err = rows.Err(); err != nil {
		return tasks, err
	}
	return tasks, nil
}

func (m mysqlDBRepo) GetAdvantages() ([]models.Advantage, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var advantages []models.Advantage

	query := `
		select 
			id, 
			IFNULL(name, ''),
			IFNULL(description, '')
		from
			advantages
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return advantages, err
	}

	for rows.Next() {
		var advantage models.Advantage
		err := rows.Scan(
			&advantage.ID,
			&advantage.Name,
			&advantage.Description,
		)
		if err != nil {
			return advantages, err
		}

		advantage.Description = template.HTML(Nl2br(string(advantage.Description)))

		advantages = append(advantages, advantage)
	}
	if err = rows.Err(); err != nil {
		return advantages, err
	}
	return advantages, nil
}

// GetPartners retrieve Partner models from base
func (m mysqlDBRepo) GetPartners() ([]models.Partner, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var partners []models.Partner

	query := `
		select 
			id, 
			IFNULL(name, ''),
			IFNULL(description, ''),
			IFNULL(image, '')
		from
			partners
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return partners, err
	}

	for rows.Next() {
		var partner models.Partner
		err := rows.Scan(
			&partner.ID,
			&partner.Name,
			&partner.Description,
			&partner.Image,
		)
		if err != nil {
			return partners, err
		}
		partners = append(partners, partner)
	}
	if err = rows.Err(); err != nil {
		return partners, err
	}
	return partners, nil
}

// GetReviews retrieve Review models from base
func (m mysqlDBRepo) GetReviews() ([]models.Review, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var reviews []models.Review

	query := `
		select 
			id, 
			IFNULL(name, ''),
			type,
			IFNULL(text, ''),
			IFNULL(icon, '')
		from
			review
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return reviews, err
	}

	for rows.Next() {
		var review models.Review
		err := rows.Scan(
			&review.ID,
			&review.Name,
			&review.Type,
			&review.Text,
			&review.Icon,
		)
		if err != nil {
			return reviews, err
		}
		reviews = append(reviews, review)
	}
	if err = rows.Err(); err != nil {
		return reviews, err
	}
	return reviews, nil
}
func (m mysqlDBRepo) GetCleanTypes() ([]models.CleanType, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var cleanTypes []models.CleanType

	query := `
		select 
			id, 
			name,
			IFNULL(description, '')
		from
			clean_type
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return cleanTypes, err
	}

	for rows.Next() {
		var cleanType models.CleanType
		err := rows.Scan(
			&cleanType.ID,
			&cleanType.Name,
			&cleanType.Description,
		)
		if err != nil {
			return cleanTypes, err
		}
		cleanTypes = append(cleanTypes, cleanType)
	}
	if err = rows.Err(); err != nil {
		return cleanTypes, err
	}
	return cleanTypes, nil
}

// Nl2br replaces newline character with html tag <br/>
func Nl2br(str string) template.HTML {
	return template.HTML(strings.Replace(str, "\n", "<br />", -1))
}

func (m mysqlDBRepo) CreateOrder(i models.Order) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
	INSERT INTO orders (
		work_type, 
		work_date, 
		workplace,
		area,
		address,
		name,
		phone,
		comment,
		status,
		utm_source,
		utm_medium,
		utm_campaign,
		utm_term,
		utm_content
	)
	VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?)
	`

	_, err := m.DB.ExecContext(ctx, query,
		i.WorkType,
		i.WorkDate,
		i.Workplace,
		i.Area,
		i.Address,
		i.Name,
		i.Phone,
		i.Comment,
		i.Status,
		i.UtmSource,
		i.UtmMedium,
		i.UtmCampaign,
		i.UtmTerm,
		i.UtmContent,
	)
	if err != nil {
		return err
	}
	return nil
}

// Authenticate authenticates user
func (m *mysqlDBRepo) Authenticate(email, testPassword string) (int, string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var id int
	var hashedPassword string

	row := m.DB.QueryRowContext(ctx, "SELECT id, password_hash FROM admin WHERE email = ?", email)
	err := row.Scan(&id, &hashedPassword)
	if err != nil {
		return id, "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(testPassword))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return 0, "", errors.New("incorrect password")
	} else if err != nil {
		return 0, "", err
	}

	return id, hashedPassword, nil
}

func (m mysqlDBRepo) GetUserByPasswordResetToken(token string) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	var user models.User
	query := `
		SELECT
			id,
			username,
			auth_key,
			password_reset_token,
			email,
			IFNULL(status, 0),
			IFNULL(created_at, 0),
			IFNULL(updated_at, 0)
		FROM
			admin
		WHERE
			password_reset_token = ?

		;`
	row := m.DB.QueryRowContext(ctx, query, token)
	err := row.Scan(
		&user.ID,
		&user.Username,
		&user.AuthKey,
		&user.PasswordResetToken,
		&user.Email,
		&user.Status,
		&user.CreatedAt,
		&user.UpdatedAt,
	)
	if err != nil {
		return user, err
	}
	return user, nil
}

// HashPassword hashes the password
func (m *mysqlDBRepo) HashPassword(token string, password string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var hashedPassword, err = bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Println("HashAndSavePassword:", err)
	}

	fmt.Println("hashedPassword", string(hashedPassword))

	query := `
	UPDATE admin 
	SET 
		password_hash = ?,
		password_reset_token = null
	WHERE password_reset_token = ?
	`
	userID, err := m.DB.ExecContext(ctx, query,
		string(hashedPassword),
		token,
	)
	fmt.Println("&userID", &userID)

	if err != nil {
		return err
	}

	return nil

}

// GetOrders retrieve Review models from base
func (m mysqlDBRepo) GetOrders(pagination models.Pagination) (models.Pagination, []models.Order, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var orders []models.Order

	_ = m.DB.QueryRow("SELECT COUNT(*) FROM orders").Scan(&pagination.Total)

	if pagination.PageSize == 0 {
		pagination.PageSize = 20
	}

	var offset int
	if pagination.CurrentPage > 1 {
		offset = pagination.PageSize * (pagination.CurrentPage - 1)
	} else {
		offset = 0
	}

	query := `
		SELECT 
			id, 
			IFNULL(work_date, ''),
			IFNULL(work_type, ''),
			IFNULL(workplace, ''),
			IFNULL(address, ''),
			IFNULL(name, ''),
			IFNULL(phone, ''),
			IFNULL(comment, ''),
			IFNULL(contacts, ''),
			IFNULL(email, ''),
			date,
			IFNULL(status, ''),
			IFNULL(utm_content, ''),
			IFNULL(utm_source, ''),
			IFNULL(utm_medium, ''),
			IFNULL(utm_campaign, ''),
			IFNULL(utm_term, '')
		FROM
			orders
			
		LIMIT ?
		OFFSET ?	
			;	
	`
	rows, err := m.DB.QueryContext(ctx, query, pagination.PageSize, offset)
	if err != nil {
		return pagination, orders, err
	}
	layout := "2006-01-02 15:04:05"
	for rows.Next() {
		var order models.Order
		var strDate string
		err := rows.Scan(
			&order.ID,
			&order.WorkDate,
			&order.WorkType,
			&order.Workplace,
			&order.Address,
			&order.Name,
			&order.Phone,
			&order.Comment,
			&order.Contacts,
			&order.Email,
			&strDate,
			&order.Status,
			&order.UtmContent,
			&order.UtmSource,
			&order.UtmMedium,
			&order.UtmCampaign,
			&order.UtmTerm,
		)
		t, err := time.Parse(layout, strDate)
		order.Date = t
		if err != nil {
			return pagination, orders, err
		}
		orders = append(orders, order)
	}
	if err = rows.Err(); err != nil {
		return pagination, orders, err
	}
	return pagination, orders, nil
}
