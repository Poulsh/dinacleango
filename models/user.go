package models

// User is model of user
type User struct {
	ID                 int
	Username           string
	AuthKey            string
	PasswordHash       string
	PasswordResetToken string
	Email              string
	Status             int
	CreatedAt          int
	UpdatedAt          int
}
