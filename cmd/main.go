package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/quadrosh/dinacleango/config"
	"github.com/quadrosh/dinacleango/driver"
	"github.com/quadrosh/dinacleango/handlers"
	"github.com/quadrosh/dinacleango/helpers"
	"github.com/quadrosh/dinacleango/mailer"
	"github.com/quadrosh/dinacleango/models"
	"github.com/quadrosh/dinacleango/render"
)

var app config.AppConfig
var session *scs.SessionManager
var infoLog *log.Logger
var errorLog *log.Logger
var vesion string

func readFlags() *config.LocalConfig {
	inProduction := flag.Bool("production", true, "Application is in production")
	ssl := flag.Bool("ssl", false, "is SSL")
	sslSert := flag.String("sslsert", "", "SSL sertificat")
	sslKey := flag.String("sslkey", "", "SSL key")

	useCache := flag.Bool("cache", true, "Use template cache")
	dbHost := flag.String("dbhost", "localhost", "Database host")
	dbName := flag.String("dbname", "", "Database name")
	dbUser := flag.String("dbuser", "", "Database user")
	dbPass := flag.String("dbpass", "", "Database password")
	dbPort := flag.String("dbport", "8889", "Database port") // 8889 or 3306
	port := flag.String("port", "8080", "Application serve port")

	mailSMTP := flag.String("mailsmtp", "localhost", "Mailer smtp port")
	mailPort := flag.Int("mailport", 25, "Mailer port")
	mailLogin := flag.String("maillogin", "", "Mailer login")
	mailPass := flag.String("mailpass", "", "Mailer password")
	mailEncrypted := flag.Bool("mailencrypted", false, "Mailer encryption")
	mailFrom := flag.String("mailfrom", "", "Mailer sender address")
	mailTo := flag.String("mailto", "", "Mailer admin address")
	smsTo := flag.String("smsto", "", "SMS order recepient")
	smsKey := flag.String("smskey", "", "SMS api key")

	flag.Parse()

	if *dbName == "" || *dbUser == "" {
		fmt.Println("Missing required flags")
		os.Exit(1)
	}

	if *ssl == true {
		if *sslSert == "" {
			fmt.Println("Missing SSL sert flag")
			os.Exit(1)
		}
		if *sslKey == "" {
			fmt.Println("Missing SSL key flag")
			os.Exit(1)
		}

	}

	app.MailSMTP = *mailSMTP
	app.MailPort = *mailPort
	app.MailLogin = *mailLogin
	app.MailPassword = *mailPass
	app.MailEncrypted = *mailEncrypted
	app.MailFrom = *mailFrom
	app.MailTo = *mailTo
	app.SmsTo = *smsTo
	app.SmsKey = *smsKey

	lConfig := config.LocalConfig{
		InProduction: *inProduction,
		UseCache:     *useCache,
		DbHost:       *dbHost,
		DbName:       *dbName,
		DbUser:       *dbUser,
		DbPass:       *dbPass,
		DbPort:       *dbPort,
		AppPort:      *port,
		SSL:          *ssl,
		SSLKey:       *sslKey,
		SSLCert:      *sslSert,
	}
	return &lConfig
}

func main() {
	version := "0.1"
	fmt.Println("App version: ", version)
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// read flags
	lConfig := readFlags()

	db, err := run(lConfig)
	if err != nil {
		log.Fatal(err)
	}
	defer db.SQL.Close()
	fmt.Println(fmt.Sprintf("Starting application on port :%s - http://localhost:%s/", lConfig.AppPort, lConfig.AppPort))

	defer close(app.MailChan)
	fmt.Println("Starting mail listener ... ")
	mailer.ListenForMail(&app)

	// _ = http.ListenAndServe(portNumber, nil)

	srv := &http.Server{
		Addr:    ":" + lConfig.AppPort,
		Handler: routes(&app),
	}

	if lConfig.SSL {
		err = srv.ListenAndServeTLS(lConfig.SSLCert, lConfig.SSLKey)
	} else {
		err = srv.ListenAndServe()
	}

	log.Fatal(err)
}

func run(lConfig *config.LocalConfig) (*driver.DB, error) {

	// types, which can be put in the session
	gob.Register(models.Page{})
	gob.Register([]models.Page{})
	gob.Register([]models.HomeSlide{})
	gob.Register([]models.Advantage{})
	gob.Register([]models.Partner{})
	gob.Register([]models.Review{})
	gob.Register(models.BlockData{})
	gob.Register([]string{})

	mailChan := make(chan models.MailData)
	app.MailChan = mailChan

	// change this to true when in production
	app.InProduction = lConfig.InProduction

	infoLog = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	app.InfoLog = infoLog

	errorLog = log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	app.ErrorLog = errorLog

	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = app.InProduction

	app.Session = session

	// connect to database
	log.Println("Connecting to database...")

	connecitonString := fmt.Sprintf("%s:%s@(%s:%s)/%s",
		lConfig.DbUser,
		lConfig.DbPass,
		lConfig.DbHost,
		lConfig.DbPort,
		lConfig.DbName)

	db, err := driver.ConnectSQL(connecitonString)
	if err != nil {
		log.Fatal("Cannot connect to database Dying...")
	}
	log.Println("Connected to database!")

	tc, err := render.CreateTemplateCache()
	if err != nil {
		log.Fatal("cannot create template cache")
		return nil, err
	}

	app.TemplateCache = tc
	app.UseCache = lConfig.UseCache

	repo := handlers.NewRepo(&app, db)

	render.NewRenderer(&app)
	handlers.NewHandlers(repo)

	helpers.NewHelpers(&app)

	// http.HandleFunc("/", handlers.Repo.Home)

	return db, nil
}
