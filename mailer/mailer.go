package mailer

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/quadrosh/dinacleango/config"
	"github.com/quadrosh/dinacleango/helpers"
	"github.com/quadrosh/dinacleango/models"

	mail "github.com/xhit/go-simple-mail/v2"
)

const smsMaxLength = 250

// ListenForMail ...
func ListenForMail(app *config.AppConfig) {
	go func() {
		for {
			msg := <-app.MailChan
			SendMsg(msg, app)
		}
	}()
}

// SendMsg ...
func SendMsg(m models.MailData, app *config.AppConfig) {
	server := mail.NewSMTPClient()
	// server.Host = "localhost"
	// server.Port = 1025
	server.KeepAlive = false
	server.ConnectTimeout = 10 * time.Second
	server.SendTimeout = 10 * time.Second

	server.Host = app.MailSMTP
	server.Port = app.MailPort
	server.Username = app.MailLogin
	server.Password = app.MailPassword
	if app.MailEncrypted {
		server.Encryption = mail.EncryptionSSL
	} else {
		server.Encryption = mail.EncryptionNone
	}

	client, err := server.Connect()
	if err != nil {
		log.Println(err)
	}
	email := mail.NewMSG()
	email.SetFrom(m.From).AddTo(m.To).SetSubject(m.Subject)
	if m.Template == "" {
		email.SetBody(mail.TextHTML, string(m.Content))
	} else {
		data, err := ioutil.ReadFile(fmt.Sprintf("./email_templates/%s", m.Template))
		if err != nil {
			app.ErrorLog.Println(err)
		}
		mailTemplate := string(data)
		msgToSend := strings.Replace(mailTemplate, "[%body%]", m.Content, 1)
		email.SetBody(mail.TextHTML, msgToSend)

	}

	err = email.Send(client)
	if err != nil {
		log.Println(err)
	}

	runes := []rune(m.Sms)
	if len(runes) >= smsMaxLength {
		m.Sms = helpers.Excerpt(m.Sms, smsMaxLength, true)
	}

	// SMS send
	url := fmt.Sprintf("https://sms.ru/sms/send?api_id=%s&to=%s&msg=%s", app.SmsKey, app.SmsTo, url.QueryEscape(m.Sms))
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	req.Header.Add("Accept", "application/json")
	_, err = http.DefaultClient.Do(req)
	if err != nil {
		log.Println("SMS sending error ", err)
	} else {
		log.Println("SMS order sent successfully")
	}

}
