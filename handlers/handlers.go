package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/quadrosh/dinacleango/config"
	"github.com/quadrosh/dinacleango/driver"
	"github.com/quadrosh/dinacleango/forms"
	"github.com/quadrosh/dinacleango/helpers"
	"github.com/quadrosh/dinacleango/models"
	"github.com/quadrosh/dinacleango/render"
	"github.com/quadrosh/dinacleango/repository"
	"github.com/quadrosh/dinacleango/repository/dbrepo"
)

// Repo the repository used by the handlers
var Repo *Repository

// Repository is the repository type
type Repository struct {
	App *config.AppConfig
	DB  repository.DatabaseRepo
}

type jsonResponse struct {
	OK      bool   `json:"ok"`
	Message string `json:"message"`
	Type    string `json:"type"`
	Markup  string `json:"markup"`
}

// NewRepo creates a new repository
func NewRepo(a *config.AppConfig, db *driver.DB) *Repository {
	return &Repository{
		App: a,
		DB:  dbrepo.NewMysqlRepo(db.SQL, a),
	}
}

// NewTestRepo creates a new test respository
func NewTestRepo(a *config.AppConfig) *Repository {
	return &Repository{
		App: a,
		DB:  dbrepo.NewTestingRepo(a),
	}
}

// NewHandlers sets the repository for handlers
func NewHandlers(r *Repository) {
	Repo = r
}

// Home is home page handler
func (m *Repository) Home(w http.ResponseWriter, r *http.Request) {

	if len(r.URL.Query().Get("utm_source")) > 0 {
		m.App.Session.Put(r.Context(), "utm_source", r.URL.Query().Get("utm_source"))
		m.App.Session.Put(r.Context(), "utm_medium", r.URL.Query().Get("utm_medium"))
		m.App.Session.Put(r.Context(), "utm_campaign", r.URL.Query().Get("utm_campaign"))
		m.App.Session.Put(r.Context(), "utm_term", r.URL.Query().Get("utm_term"))
		m.App.Session.Put(r.Context(), "utm_content", r.URL.Query().Get("utm_content"))
	}

	pages, err := m.DB.GetHomePages()
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	slides, err := m.DB.GetHomeSlides()
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	defaultService, err := m.DB.GetPageByID(models.PageSupportingID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	advantages, err := m.DB.GetAdvantages()
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	partners, err := m.DB.GetPartners()
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	reviews, err := m.DB.GetReviews()
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	data := make(map[string]interface{})

	data["home"] = pages[0]
	data["pages"] = pages
	data["slides"] = slides
	data["advantages"] = advantages
	data["partners"] = partners
	data["reviews"] = reviews
	data["defaultServiceData"] = &models.BlockData{
		Head:             defaultService.Pagehead,
		Text:             defaultService.Pagedescription,
		IsTypeSupporting: true,
		Button1Url:       "/support-cleaning-prices-json",
		Button1Name:      "Цены",
		Button2Url:       "/support-cleaning-vhodit-json",
		Button2Name:      "Что входит",
	}

	cleanTypes, err := m.DB.GetCleanTypes()
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	types := make([]string, 4)
	for i, item := range cleanTypes {
		types[i] = item.Name
	}
	data["cleanOptions"] = types

	data["homeOptions"] = []string{"квартира", "коттедж", "другое"}
	data["areaOptions"] = []string{"?", "меньше 10-ти", "10", "15", "20", "25",
		"30", "35", "40", "45", "50", "60", "70", "80", "90", "100", "110", "120", "150",
		"200", "больше 200 кв. м."}

	now := time.Now()
	_, month0, day0 := now.Date()
	weekday0 := now.Weekday()

	now1 := now.AddDate(0, 0, 1)
	_, month1, day1 := now1.Date()
	weekday1 := now1.Weekday()

	now2 := now.AddDate(0, 0, 2)
	_, month2, day2 := now2.Date()
	weekday2 := now2.Weekday()

	now3 := now.AddDate(0, 0, 3)
	_, month3, day3 := now3.Date()
	weekday3 := now3.Weekday()

	now4 := now.AddDate(0, 0, 4)
	_, month4, day4 := now1.Date()
	weekday4 := now4.Weekday()

	data["dataOptions"] = []string{
		constructDate(day0, int(month0-1), int(weekday0)),
		constructDate(day1, int(month1-1), int(weekday1)),
		constructDate(day2, int(month2-1), int(weekday2)),
		constructDate(day3, int(month3-1), int(weekday3)),
		constructDate(day4, int(month4-1), int(weekday4)),
		"другая дата",
	}

	// log.Printf("data %v\n", data)

	// сохраняем в сессию для отриcовки формы заказа
	m.App.Session.Put(r.Context(), "home", pages[0])
	m.App.Session.Put(r.Context(), "pages", pages)
	m.App.Session.Put(r.Context(), "slides", slides)
	m.App.Session.Put(r.Context(), "advantages", advantages)
	m.App.Session.Put(r.Context(), "partners", partners)
	m.App.Session.Put(r.Context(), "reviews", reviews)
	m.App.Session.Put(r.Context(), "defaultServiceData", data["defaultServiceData"])
	m.App.Session.Put(r.Context(), "cleanOptions", data["cleanOptions"])
	m.App.Session.Put(r.Context(), "homeOptions", data["homeOptions"])
	m.App.Session.Put(r.Context(), "areaOptions", data["areaOptions"])
	m.App.Session.Put(r.Context(), "dataOptions", data["dataOptions"])

	form := forms.New(r.PostForm)
	form.Required("phone")
	form.MinLength("phone", 7)
	form.IsEmail("email")

	render.RenderTemplate(w, r, "home.page.tmpl", &models.TemplateData{
		Data:  data,
		Form:  form,
		Flash: "HOME Flash message",
	})
}

func constructDate(day, month, weekDay int) string {
	// const months = []string{"январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"}

	var months = []string{"января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"}

	var weekDays = []string{"понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"}
	return weekDays[weekDay] + ", " + strconv.Itoa(day) + " " + months[month]
}

// CleanSupportDefaultJSON handles json request with partial rerender of template
func (m *Repository) CleanSupportDefaultJSON(w http.ResponseWriter, r *http.Request) {

	page, err := m.DB.GetPageByID(models.PageSupportingID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_info.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_info.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, &models.BlockData{
		Head:             page.Pagehead,
		Text:             page.Pagedescription,
		IsTypeSupporting: true,
		Button1Url:       "/support-cleaning-prices-json",
		Button1Name:      "Цены",
		Button2Url:       "/support-cleaning-vhodit-json",
		Button2Name:      "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanSupportPricesJSON handles json request with partial rerender of template
func (m *Repository) CleanSupportPricesJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageSupportingID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	prices, err := m.DB.GetPrices(models.TypeSupporting)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_price.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_price.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:             page.Pagehead,
		Head2:            "Цены на поддерживающую уборку:",
		Text:             page.Pagedescription,
		Prices:           prices,
		IsTypeSupporting: true,
		Button1Url:       "/support-cleaning-prices-json",
		Button1Name:      "Цены",
		Button1Active:    true,
		Button2Url:       "/support-cleaning-vhodit-json",
		Button2Name:      "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanSupportVhoditJSON handles json request with partial rerender of template supporting vhodit
func (m *Repository) CleanSupportVhoditJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageSupportingID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	tasks, err := m.DB.GetTasks(models.TypeSupporting)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	t := template.New("clean_tasks.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_tasks.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, &models.BlockData{
		Head:             page.Pagehead,
		Head2:            "в поддерживающую уборку входит:",
		Text:             page.Pagedescription,
		Tasks:            tasks,
		IsTypeSupporting: true,
		Button1Url:       "/support-cleaning-prices-json",
		Button1Name:      "Цены",
		Button2Active:    true,
		Button2Url:       "/support-cleaning-vhodit-json",
		Button2Name:      "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanGeneralDefaultJSON handles json request with partial rerender of template
func (m *Repository) CleanGeneralDefaultJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageGeneralID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_info.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_info.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:          page.Pagehead,
		Text:          page.Pagedescription,
		IsTypeGeneral: true,
		Button1Url:    "/general-cleaning-prices-json",
		Button1Name:   "Цены",
		Button2Url:    "/general-cleaning-vhodit-json",
		Button2Name:   "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanGeneralPricesJSON handles json request with partial rerender of template
func (m *Repository) CleanGeneralPricesJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageGeneralID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	prices, err := m.DB.GetPrices(models.TypeGeneral)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_price.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_price.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:          page.Pagehead,
		Head2:         "Цены на генеральную уборку:",
		Text:          page.Pagedescription,
		Prices:        prices,
		IsTypeGeneral: true,
		Button1Url:    "/general-cleaning-prices-json",
		Button1Name:   "Цены",
		Button1Active: true,
		Button2Url:    "/general-cleaning-vhodit-json",
		Button2Name:   "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanGeneralVhoditJSON handles json request with partial rerender of template supporting vhodit
func (m *Repository) CleanGeneralVhoditJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageGeneralID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	tasks, err := m.DB.GetTasks(models.TypeGeneral)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	t := template.New("clean_tasks.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_tasks.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, &models.BlockData{
		Head:          page.Pagehead,
		Head2:         "в генеральную уборку входит:",
		Text:          page.Pagedescription,
		Tasks:         tasks,
		IsTypeGeneral: true,
		Button1Url:    "/general-cleaning-prices-json",
		Button1Name:   "Цены",
		Button2Active: true,
		Button2Url:    "/general-cleaning-vhodit-json",
		Button2Name:   "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanAfterReconstructionDefaultJSON handles json request with partial rerender of template
func (m *Repository) CleanAfterReconstructionDefaultJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageAfterReconstructionID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_info.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_info.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:                      page.Pagehead,
		Text:                      page.Pagedescription,
		IsTypeAfterReconstruction: true,
		Button1Url:                "/after-reconstruction-cleaning-prices-json",
		Button1Name:               "Цены",
		Button2Url:                "/after-reconstruction-cleaning-vhodit-json",
		Button2Name:               "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanAfterReconstructionPricesJSON handles json request with partial rerender of template
func (m *Repository) CleanAfterReconstructionPricesJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageAfterReconstructionID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	prices, err := m.DB.GetPrices(models.TypeAfterReconstruction)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_price.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_price.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:                      page.Pagehead,
		Head2:                     "Цены на уборку после ремонта:",
		Text:                      page.Pagedescription,
		Prices:                    prices,
		IsTypeAfterReconstruction: true,
		Button1Url:                "/after-reconstruction-cleaning-prices-json",
		Button1Name:               "Цены",
		Button1Active:             true,
		Button2Url:                "/after-reconstruction-cleaning-vhodit-json",
		Button2Name:               "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanAfterReconstructionVhoditJSON handles json request with partial rerender of template
func (m *Repository) CleanAfterReconstructionVhoditJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageAfterReconstructionID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	tasks, err := m.DB.GetTasks(models.TypeAfterReconstruction)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	t := template.New("clean_tasks.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_tasks.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, &models.BlockData{
		Head:                      page.Pagehead,
		Head2:                     "в уборку после ремонта входит:",
		Text:                      page.Pagedescription,
		Tasks:                     tasks,
		IsTypeAfterReconstruction: true,
		Button1Url:                "/after-reconstruction-cleaning-prices-json",
		Button1Name:               "Цены",
		Button2Active:             true,
		Button2Url:                "/after-reconstruction-cleaning-vhodit-json",
		Button2Name:               "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanWindowsDefaultJSON handles json request with partial rerender of template
func (m *Repository) CleanWindowsDefaultJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageWindowsID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_info.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_info.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:          page.Pagehead,
		Text:          page.Pagedescription,
		IsTypeWindows: true,
		Button1Url:    "/windows-cleaning-prices-json",
		Button1Name:   "Цены",
		Button2Url:    "/windows-cleaning-vhodit-json",
		Button2Name:   "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanWindowsPricesJSON handles json request with partial rerender of template
func (m *Repository) CleanWindowsPricesJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageWindowsID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	prices, err := m.DB.GetPrices(models.TypeWindows)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	t := template.New("clean_price.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_price.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, &models.BlockData{
		Head:          page.Pagehead,
		Head2:         "Цены на мойку окон:",
		Text:          page.Pagedescription,
		Prices:        prices,
		IsTypeWindows: true,
		Button1Url:    "/windows-cleaning-prices-json",
		Button1Name:   "Цены",
		Button1Active: true,
		Button2Url:    "/windows-cleaning-vhodit-json",
		Button2Name:   "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// CleanWindowsVhoditJSON handles json request with partial rerender of template
func (m *Repository) CleanWindowsVhoditJSON(w http.ResponseWriter, r *http.Request) {
	page, err := m.DB.GetPageByID(models.PageWindowsID)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	tasks, err := m.DB.GetTasks(models.TypeWindows)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	t := template.New("clean_tasks.block.tmpl")
	t, err = t.ParseFiles("./templates/clean_tasks.block.tmpl")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, &models.BlockData{
		Head:          page.Pagehead,
		Head2:         "в мойку окон входит:",
		Text:          page.Pagedescription,
		Tasks:         tasks,
		IsTypeWindows: true,
		Button1Url:    "/windows-cleaning-prices-json",
		Button1Name:   "Цены",
		Button2Active: true,
		Button2Url:    "/windows-cleaning-vhodit-json",
		Button2Name:   "Что входит",
	})
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	templateString := tpl.String()
	resp := jsonResponse{
		OK:     true,
		Markup: html.UnescapeString(templateString),
	}
	out, err := json.MarshalIndent(resp, "", "     ")
	if err != nil {
		helpers.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

// Nl2br replace \n by <br /> in string
func Nl2br(str string) string {
	return strings.Replace(str, "\n", "<br />", -1)
}

// SubmitOrderMain save order from main form in bd and send notifications
func (m *Repository) SubmitOrderMain(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "can't parse form")
		log.Println(helpers.FileLine() + " can't parse form")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	var order models.Order
	order.WorkType = r.Form.Get("work_type")
	order.WorkDate = r.Form.Get("work_date")
	order.Phone = r.Form.Get("phone")
	order.Address = r.Form.Get("address")
	order.Workplace = r.Form.Get("workplace")
	order.Area = r.Form.Get("area")
	order.Comment = r.Form.Get("comment")

	log.Println("order: ", order)

	form := forms.New(r.PostForm)

	form.Required("phone")
	form.MinLength("phone", 7)

	data := make(map[string]interface{})
	var ok bool

	data["home"], ok = m.App.Session.Get(r.Context(), "home").(models.Page)
	if !ok {
		log.Println("cannot get item from session")
		m.App.ErrorLog.Println("Can't get something from session")
		m.App.Session.Put(r.Context(), "error", "Не получена дата из сессии")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	data["pages"], _ = m.App.Session.Get(r.Context(), "pages").([]models.Page)
	data["slides"], _ = m.App.Session.Get(r.Context(), "slides").([]models.HomeSlide)
	data["advantages"], _ = m.App.Session.Get(r.Context(), "advantages").([]models.Advantage)
	data["partners"], _ = m.App.Session.Get(r.Context(), "partners").([]models.Partner)
	data["reviews"], _ = m.App.Session.Get(r.Context(), "reviews").([]models.Review)
	data["defaultServiceData"], _ = m.App.Session.Get(r.Context(), "defaultServiceData").(models.BlockData)
	data["cleanOptions"], _ = m.App.Session.Get(r.Context(), "cleanOptions").([]string)
	data["homeOptions"], _ = m.App.Session.Get(r.Context(), "homeOptions").([]string)
	data["areaOptions"], _ = m.App.Session.Get(r.Context(), "areaOptions").([]string)
	data["dataOptions"], _ = m.App.Session.Get(r.Context(), "dataOptions").([]string)
	data["order"] = order

	if !form.Valid() {

		var templError string

		if pErrors, isErr := form.Errors["phone"]; isErr {
			var errText string
			for i, err := range pErrors {
				if i == 0 {
					errText = err
				} else {
					errText = errText + "<br> " + err
				}
			}
			// templError = "Ошибка в поле телефон:<br> " + errText
			m.App.Session.Put(r.Context(), "error", "Ошибка в поле телефон:<br> "+errText)
		} else {
			m.App.Session.Put(r.Context(), "error", "Форма заполнена не полностью")
		}

		// log.Printf("form.Errors.Get(phone) %v", form.Errors.Get("phone"))
		// log.Printf("form %#v\n", form)

		log.Printf("templError %v\n", templError)

		render.RenderTemplate(w, r, "make_order.page.tmpl", &models.TemplateData{
			Form: form,
			Data: data,
		})

		return
	}

	order.UtmSource = m.App.Session.GetString(r.Context(), "utm_source")
	order.UtmMedium = m.App.Session.GetString(r.Context(), "utm_medium")
	order.UtmCampaign = m.App.Session.GetString(r.Context(), "utm_campaign")
	order.UtmTerm = m.App.Session.GetString(r.Context(), "utm_term")
	order.UtmContent = m.App.Session.GetString(r.Context(), "utm_content")

	err = m.DB.CreateOrder(order)
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "ошибка сохранения")
		log.Printf("ошибка сохранения %v", err)
		return
	}

	// TODO send email and SMS notification
	// send notification - to owner

	orderMessage := fmt.Sprintf("телефон %s", order.Phone)
	if order.WorkDate != "" {
		orderMessage = fmt.Sprintf("%s, дата %s", orderMessage, order.WorkDate)
	}
	if order.WorkType != "" {
		orderMessage = fmt.Sprintf("%s, %s", orderMessage, order.WorkType)
	}
	if order.Address != "" {
		orderMessage = fmt.Sprintf("%s, %s", orderMessage, order.Address)
	}
	if order.Workplace != "" {
		orderMessage = fmt.Sprintf("%s, %s", orderMessage, order.Workplace)
	}
	if order.Area != "" && order.Area != "?" {
		orderMessage = fmt.Sprintf("%s, площадь:%sм", orderMessage, order.Area)
	}
	if order.Comment != "" {
		orderMessage = fmt.Sprintf("%s, комментарий:%s", orderMessage, order.Comment)
	}
	htmlMessage := fmt.Sprintf(`
	<strong>Заказ на уборку </strong> <br>
	  %s <br>
	`, orderMessage)

	smsMessage := fmt.Sprintf(`Заказ на уборку Dinacleaning. %s`, orderMessage)

	msg := models.MailData{
		To:      "quadrosh@gmail.com",
		From:    "sender@dinacleaning.ru",
		Subject: "New Order",
		Content: htmlMessage,
		Sms:     smsMessage,
	}

	m.App.MailChan <- msg

	render.RenderTemplate(w, r, "thanks_for_order.page.tmpl", &models.TemplateData{
		Form:  form,
		Data:  data,
		Modal: "Заявка отправлена, <br> мы свяжемся с Вами в ближайшее время.",
	})

}

// PostSubmitPhoneForm save order from phone form in bd and send notifications
func (m *Repository) PostSubmitPhoneForm(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "can't parse form")
		log.Println(helpers.FileLine() + " can't parse form")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	var order models.Order
	order.Phone = r.Form.Get("phone")

	// log.Println("order: ", order)

	form := forms.New(r.PostForm)

	form.Required("phone")

	if !form.Valid() {

		// var templError string

		if pErrors, isErr := form.Errors["phone"]; isErr {
			var errText string
			for i, err := range pErrors {
				if i == 0 {
					errText = err
				} else {
					errText = errText + "<br> " + err
				}
			}
			// templError = "Ошибка в поле телефон:<br> " + errText
			m.App.Session.Put(r.Context(), "error", "Ошибка в поле телефон:<br> "+errText)
		} else {
			m.App.Session.Put(r.Context(), "error", "Форма заполнена не полностью")
		}

		// log.Printf("templError %v\n", templError)

		return
	}

	order.UtmSource = m.App.Session.GetString(r.Context(), "utm_source")
	order.UtmMedium = m.App.Session.GetString(r.Context(), "utm_medium")
	order.UtmCampaign = m.App.Session.GetString(r.Context(), "utm_campaign")
	order.UtmTerm = m.App.Session.GetString(r.Context(), "utm_term")
	order.UtmContent = m.App.Session.GetString(r.Context(), "utm_content")

	err = m.DB.CreateOrder(order)
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "ошибка сохранения")
		log.Printf("ошибка сохранения заявки: %v", err)
		return
	}

	// TODO send SMS notification
	htmlMessage := fmt.Sprintf(`
		<strong>Заказ на уборку </strong> <br>
		 телефон %s 
	`,
		order.Phone)
	smsMessage := fmt.Sprintf(`Заказ на уборку Dinacleaning. Телефон %s`,
		order.Phone)
	msg := models.MailData{
		To:      m.App.MailTo,
		From:    m.App.MailFrom,
		Subject: "New Order",
		Content: htmlMessage,
		Sms:     smsMessage,
	}

	m.App.MailChan <- msg

	resp := jsonResponse{
		OK:      true,
		Message: "Заявка отправлена, <br> мы свяжемся с Вами в ближайшее время.",
	}
	out, _ := json.MarshalIndent(resp, "", "     ")

	w.Header().Set("Content-Type", "application/json")
	w.Write(out)

}

// FaviconHandler handles favicon request
func (m *Repository) FaviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./assets/client/img/favicon/favicon.ico")
}

// ShowLogin shows login form page
func (m *Repository) ShowLogin(w http.ResponseWriter, r *http.Request) {
	render.RenderTemplate(w, r, "login.page.tmpl", &models.TemplateData{
		Form: forms.New(nil),
	})
}

// PostShowLogin handles logging the user in
func (m *Repository) PostShowLogin(w http.ResponseWriter, r *http.Request) {
	_ = m.App.Session.RenewToken(r.Context())

	err := r.ParseForm()
	if err != nil {
		log.Println(err)
	}

	email := r.Form.Get("email")
	password := r.Form.Get("password")
	form := forms.New(r.PostForm)
	form.Required("email", "password")
	form.IsEmail("email")
	if !form.Valid() {
		render.RenderTemplate(w, r, "login.page.tmpl", &models.TemplateData{
			Form: form,
		})
		return
	}

	id, _, err := m.DB.Authenticate(email, password)
	if err != nil {
		log.Println(err)
		m.App.Session.Put(r.Context(), "error", "Неверный логин или пароль")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	m.App.Session.Put(r.Context(), "user_id", id)
	m.App.Session.Put(r.Context(), "flash", "Вы успешно вошли")
	http.Redirect(w, r, "/admin/dashboard", http.StatusSeeOther)
}

// Logout logged out user
func (m *Repository) Logout(w http.ResponseWriter, r *http.Request) {
	_ = m.App.Session.Destroy(r.Context())
	_ = m.App.Session.RenewToken(r.Context())
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// AdminDashboard ...
func (m *Repository) AdminDashboard(w http.ResponseWriter, r *http.Request) {
	fmt.Println("AdminDashboard()")
	render.RenderTemplate(w, r, "admin-dashboard.page.tmpl", &models.TemplateData{})
}

// PasswordReset ...
func (m *Repository) PasswordReset(w http.ResponseWriter, r *http.Request) {

	data := make(map[string]interface{})
	token := chi.URLParam(r, "token")
	log.Println("token: ", token)
	if token == "" {
		log.Fatal("token not found")
	}

	user, err := m.DB.GetUserByPasswordResetToken(token)
	if err != nil {
		log.Println(err)
		data["error"] = "Пользователь не найден или ссылка уже использована"
	}
	// log.Println("user", user)

	if user.ID != 0 {
		data["token"] = user.PasswordResetToken
		data["username"] = user.Username
	}

	err = render.RenderTemplate(w, r, "password-reset.page.tmpl", &models.TemplateData{
		Data: data,
	})
	if err != nil {
		log.Fatal(err)
	}
}

// PasswordResetJSON ...
func (m *Repository) PasswordResetJSON(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println(helpers.FileLine() + "cant parse form")
		// http.Redirect(w, r, "/")
	}

	token := r.Form.Get("token")
	password := r.Form.Get("password")

	data := make(map[string]interface{})

	err = m.DB.HashPassword(token, password)
	if err != nil {
		log.Println(err)
		data["error"] = "Internal server error"
		err = render.RenderTemplate(w, r, "password-reset.page.tmpl", &models.TemplateData{
			Data: data,
		})
		if err != nil {
			log.Fatal(err)
		}
	}
	// m.App.Session.Put(r.Context(), "user_id", id)
	m.App.Session.Put(r.Context(), "flash", "Вы успешно изменили пароль")
	http.Redirect(w, r, "/login", http.StatusSeeOther)

}

// AdminOrders shows all reservation in admin tool
func (m *Repository) AdminOrders(w http.ResponseWriter, r *http.Request) {

	page := 1
	size := 20
	var err error

	if urlPage := r.URL.Query().Get("page"); urlPage != "" {
		page, err = strconv.Atoi(urlPage)
		if err != nil {
			log.Println(err)
		}

		if urlSize := r.URL.Query().Get("size"); urlSize != "" {
			size, err = strconv.Atoi(urlSize)
			if err != nil {
				log.Println(err)
			}
		}

	}

	reqPagination := models.Pagination{
		PageSize:    size,
		CurrentPage: page,
	}
	pagination, orders, err := m.DB.GetOrders(reqPagination)
	if err != nil {
		helpers.ServerError(w, err)
		return
	}

	data := make(map[string]interface{})
	data["orders"] = orders

	pagination.Construct()

	data["pagination"] = pagination
	render.RenderTemplate(w, r, "admin-orders.page.tmpl", &models.TemplateData{
		Data: data,
	})
}
