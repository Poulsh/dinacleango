package forms

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestForm_Valid(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)
	form := New(r.PostForm)

	isValid := form.Valid()
	if !isValid {
		t.Error("got invalid when should have been valid")
	}
}

func TestForm_Required(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)
	form := New(r.PostForm)

	form.Required("a", "b", "c")
	if form.Valid() {
		t.Error("form shows valid when required fields missing")
	}

	postedData := url.Values{}
	postedData.Add("a", "a")
	postedData.Add("b", "a")
	postedData.Add("c", "a")

	r, _ = http.NewRequest("POST", "/whatever", nil)
	r.PostForm = postedData
	form = New(r.PostForm)
	form.Required("a", "b", "c")
	if !form.Valid() {
		t.Error("shows does not have required fields when it does")
	}
}

func TestForm_Has(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)

	postedData := url.Values{}
	postedData.Add("somename", "somevalue")
	r.PostForm = postedData

	form := New(r.PostForm)

	form.Has("somename")

	if !form.Valid() {
		t.Error("not found field somename when must be")
	}

	form.Has("anothername")
	if form.Valid() {
		t.Error("found field anothername when it must not be")
	}
}

func TestForm_MinLength(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)

	postedData := url.Values{}

	text := "somevalue fdfsdf"
	fieldName := "somename"
	postedData.Add(fieldName, text)
	r.PostForm = postedData

	form := New(r.PostForm)

	form.MinLength(fieldName, len(text))

	if !form.Valid() {
		t.Error("min length not valid when must be")
	}
	// checking Get
	isError := form.Errors.Get(fieldName)
	if isError != "" {
		t.Error("should not have an error, but got one")
	}

	form.MinLength(fieldName, (len(text) + 1))
	if form.Valid() {
		t.Error("min length valid when must not be")
	}

	// checking Get
	isError = form.Errors.Get(fieldName)
	if isError == "" {
		t.Error("should have an error, but did not get one ", isError)
	}

}

func TestForm_IsEmail(t *testing.T) {
	postedData := url.Values{}

	validFieldName := "email"
	notValidFieldName := "email_not_valid"

	postedData.Add(validFieldName, "adsf@asdfasdf.df")
	postedData.Add(notValidFieldName, "asdfasdfasdf.dfs")

	form := New(postedData)

	form.IsEmail(validFieldName)
	if !form.Valid() {
		t.Error("field email is not valid when should be")
	}

	form.IsEmail(notValidFieldName)
	if form.Valid() {
		t.Error("field email is valid when should be not")
	}

	postedData = url.Values{}
	form = New(postedData)
	form.IsEmail("not_existent_field")
	if form.Valid() {
		t.Error("shows is email on not existent field")
	}
}
