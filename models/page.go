package models

// Page is data of page
type Page struct {
	ID              int
	Hrurl           string
	SeoInsert       string
	Title           string
	Description     string
	Keywords        string
	Pagehead        string
	Pagedescription string
}
