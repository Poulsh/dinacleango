package main

import (
	"log"
	"os"
	"testing"

	"github.com/quadrosh/dinacleango/config"
)

func TestRun(t *testing.T) {

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	app.MailSMTP = "localhost"
	app.MailPort = 25
	app.MailLogin = ""
	app.MailPassword = ""
	app.MailEncrypted = false
	app.MailFrom = "from@test.ru"
	app.MailTo = "to@test.ru"

	lConfig := config.LocalConfig{
		InProduction: false,
		UseCache:     true,
		DbHost:       "localhost",
		DbName:       "dinacleaningsrv",
		DbUser:       "tb303",
		DbPass:       "303",
		DbPort:       "3306", //  8889 or 3306
		AppPort:      "8080",
	}

	_, err := run(&lConfig)
	if err != nil {
		t.Error("failed run()")
	}
}

// var ssl = flag.Bool("ssl", false, "SSL sertificat")
var ssl bool

func TestReadFlags(t *testing.T) {

	os.Args = []string{"test",
		"-dbname=dataBaseName",
		"-dbuser=dataBaseUser",
		"-ssl=true",
		"-sslsert=sslsert1234",
		"-sslkey=sslkey1234",
	}

	// fmt.Printf("os.Args %#v", os.Args)

	lConfig := readFlags()

	if lConfig.DbName != "dataBaseName" {
		t.Error("dbname flag not parsed well")
	}
	if lConfig.DbUser != "dataBaseUser" {
		t.Error("dbuser flag not parsed well")
	}
	if lConfig.SSL != true {
		t.Error("ssl flag not parsed well")
	}
	if lConfig.SSLCert != "sslsert1234" {
		t.Error("sslsert flag not parsed well")
	}
	if lConfig.SSLKey != "sslkey1234" {
		t.Error("sslkey flag not parsed well")
	}

	if lConfig.DbName == "" || lConfig.DbUser == "" {
		// fmt.Println("Missing required flags")
		// os.Exit(1)
	}
	// fmt.Printf("lConfig %#v", lConfig)

}
