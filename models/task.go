package models

// Task ...
type Task struct {
	ID               int
	ParentID         int
	Name             string
	Description      string
	Price            int
	PriceDescription string
	Image            string
}
