package dbrepo

import (
	"github.com/quadrosh/dinacleango/models"
)

func (m *testDBRepo) GetHomePages() ([]models.Page, error) {

	var pages []models.Page
	pages = append(pages, models.Page{
		ID:              1,
		Hrurl:           "home",
		SeoInsert:       "",
		Title:           "",
		Description:     "",
		Keywords:        "",
		Pagehead:        "",
		Pagedescription: "",
	})
	return pages, nil
}

func (m *testDBRepo) GetHomeSlides() ([]models.HomeSlide, error) {
	var slides []models.HomeSlide
	return slides, nil
}

func (m *testDBRepo) GetPrices(cleanType int) ([]models.Price, error) {
	var prices []models.Price
	return prices, nil
}

func (m *testDBRepo) GetPageByID(id int) (models.Page, error) {
	var page models.Page
	return page, nil
}

func (m *testDBRepo) GetTasks(cleanType int) ([]models.Task, error) {
	var tasks []models.Task
	return tasks, nil
}

func (m *testDBRepo) GetAdvantages() ([]models.Advantage, error) {
	var advantages []models.Advantage
	return advantages, nil
}

// GetPartners retrieve Partner models from base
func (m *testDBRepo) GetPartners() ([]models.Partner, error) {
	var partners []models.Partner
	return partners, nil
}

// GetReviews retrieve Review models from base
func (m *testDBRepo) GetReviews() ([]models.Review, error) {
	var reviews []models.Review
	return reviews, nil
}
func (m *testDBRepo) GetCleanTypes() ([]models.CleanType, error) {
	var cleanTypes []models.CleanType
	return cleanTypes, nil
}

func (m *testDBRepo) CreateOrder(i models.Order) error {
	return nil
}

// Authenticate authenticates user
func (m *testDBRepo) Authenticate(email, testPassword string) (int, string, error) {
	var id int
	var hashedPassword string
	return id, hashedPassword, nil
}

func (m testDBRepo) GetUserByPasswordResetToken(token string) (models.User, error) {
	var user models.User
	return user, nil
}

// HashPassword hashes the password
func (m *testDBRepo) HashPassword(token string, password string) error {
	return nil
}

func (m testDBRepo) GetOrders(pagination models.Pagination) (models.Pagination, []models.Order, error) {
	var orders []models.Order
	for i := 0; i < 10; i++ {
		var order models.Order
		order.ID = i
		order.Name = "Brad"
		orders = append(orders, order)
	}
	return pagination, orders, nil
}
