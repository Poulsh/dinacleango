package models

// Review ...
type Review struct {
	ID   int
	Name string
	Type string
	Text string
	Icon string
}
