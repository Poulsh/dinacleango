package models

// HomeSlide ...
type HomeSlide struct {
	ID        int
	Title     string
	Lead      string
	Lead2     string
	Text      string
	Image     string
	Promolink string
	Promoname string
}
