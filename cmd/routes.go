package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/quadrosh/dinacleango/config"
	"github.com/quadrosh/dinacleango/handlers"
)

func routes(app *config.AppConfig) http.Handler {
	mux := chi.NewRouter()
	mux.Use(middleware.Recoverer)
	mux.Use(NoSurf)
	mux.Use(SessionLoad)

	mux.Get("/", handlers.Repo.Home)
	mux.Get("/home", handlers.Repo.Home)

	mux.Get("/favicon.ico", handlers.Repo.FaviconHandler)

	mux.Post("/windows-cleaning-json", handlers.Repo.CleanWindowsDefaultJSON)
	mux.Post("/windows-cleaning-prices-json", handlers.Repo.CleanWindowsPricesJSON)
	mux.Post("/windows-cleaning-vhodit-json", handlers.Repo.CleanWindowsVhoditJSON)

	mux.Post("/after-reconstruction-cleaning-json", handlers.Repo.CleanAfterReconstructionDefaultJSON)
	mux.Post("/after-reconstruction-cleaning-prices-json", handlers.Repo.CleanAfterReconstructionPricesJSON)
	mux.Post("/after-reconstruction-cleaning-vhodit-json", handlers.Repo.CleanAfterReconstructionVhoditJSON)

	mux.Post("/general-cleaning-json", handlers.Repo.CleanGeneralDefaultJSON)
	mux.Post("/general-cleaning-prices-json", handlers.Repo.CleanGeneralPricesJSON)
	mux.Post("/general-cleaning-vhodit-json", handlers.Repo.CleanGeneralVhoditJSON)

	mux.Post("/support-cleaning-json", handlers.Repo.CleanSupportDefaultJSON)
	mux.Post("/support-cleaning-prices-json", handlers.Repo.CleanSupportPricesJSON)
	mux.Post("/support-cleaning-vhodit-json", handlers.Repo.CleanSupportVhoditJSON)
	mux.Post("/submit-order", handlers.Repo.SubmitOrderMain)
	mux.Get("/submit-order", handlers.Repo.SubmitOrderMain)
	mux.Post("/submit-phone-order", handlers.Repo.PostSubmitPhoneForm)

	mux.Get("/login", handlers.Repo.ShowLogin)
	mux.Post("/login", handlers.Repo.PostShowLogin)
	mux.Get("/logout", handlers.Repo.Logout)
	mux.Get("/password-reset/{token}", handlers.Repo.PasswordReset)
	mux.Post("/password-reset-handle", handlers.Repo.PasswordResetJSON)

	fileServer := http.FileServer(http.Dir("./assets/"))
	mux.Handle("/assets/*", http.StripPrefix("/assets", fileServer))

	mux.Route("/admin", func(mux chi.Router) {
		mux.Use(Auth)
		mux.Get("/dashboard", handlers.Repo.AdminDashboard)
		mux.Get("/orders", handlers.Repo.AdminOrders)
	})
	return mux
}
