package render

import (
	"encoding/gob"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/quadrosh/dinacleango/config"
	"github.com/quadrosh/dinacleango/models"
)

var session *scs.SessionManager
var testApp config.AppConfig

// TestMain runs before any other code
func TestMain(m *testing.M) {

	// types, which can be put in the session
	gob.Register(models.Page{})
	gob.Register([]models.Page{})
	gob.Register([]models.HomeSlide{})
	gob.Register([]models.Advantage{})
	gob.Register([]models.Partner{})
	gob.Register([]models.Review{})
	gob.Register(models.BlockData{})
	gob.Register([]string{})

	// mailChan := make(chan models.MailData)
	// app.MailChan = mailChan

	testApp.InProduction = false

	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = false

	testApp.Session = session

	app = &testApp

	os.Exit(m.Run())
}

type myWriter struct{}

func (tw *myWriter) Header() http.Header {
	var h http.Header
	return h
}

func (tw *myWriter) WriteHeader(statusCode int) {

}

func (tw *myWriter) Write(b []byte) (int, error) {
	l := len(b)
	return l, nil
}
