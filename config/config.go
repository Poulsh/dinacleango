package config

import (
	"html/template"
	"log"

	"github.com/alexedwards/scs/v2"
	"github.com/quadrosh/dinacleango/models"
)

// AppConfig is application config
type AppConfig struct {
	UseCache      bool
	TemplateCache map[string]*template.Template
	InfoLog       *log.Logger
	ErrorLog      *log.Logger
	InProduction  bool
	Session       *scs.SessionManager
	MailChan      chan models.MailData
	MailSMTP      string
	MailPort      int
	MailLogin     string
	MailPassword  string
	MailEncrypted bool
	MailFrom      string
	MailTo        string
	SmsTo         string
	SmsKey        string
}

// LocalConfig is local configuration
type LocalConfig struct {
	InProduction bool
	UseCache     bool
	SSL          bool
	SSLCert      string
	SSLKey       string
	DbHost       string
	DbName       string
	DbUser       string
	DbPass       string
	DbPort       string
	AppPort      string
}
