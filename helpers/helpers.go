package helpers

import (
	"fmt"
	"log"
	"net/http"
	"reflect"
	"runtime"
	"runtime/debug"

	"github.com/quadrosh/dinacleango/config"
)

var app *config.AppConfig

// NewHelpers sets up app config for helpers
func NewHelpers(a *config.AppConfig) {
	app = a
}

// ClientError ...
func ClientError(w http.ResponseWriter, status int) {
	app.InfoLog.Println("Client error with status of ", status)
	http.Error(w, http.StatusText(status), status)
}

// ServerError ...
func ServerError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.ErrorLog.Println(trace)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// PrintStruct prints struct in console each field in line
func PrintStruct(name string, s interface{}) {
	v := reflect.ValueOf(s)
	typeOfS := v.Type()
	fmt.Printf("%v\n", name)
	for i := 0; i < v.NumField(); i++ {
		fmt.Printf("      %s\t  : %v\n", typeOfS.Field(i).Name, v.Field(i).Interface())
	}
}

// FileLine ...
func FileLine() string {
	_, fileName, fileLine, ok := runtime.Caller(1)
	var s string
	if ok {
		s = fmt.Sprintf("%s:%d", fileName, fileLine)
	} else {
		s = ""
	}
	return s
}

// HandleError ...
func HandleError(err error) (b bool) {
	if err != nil {
		// notice that we're using 1, so it will actually log where
		// the error happened, 0 = this function, we don't want that.
		_, fn, line, _ := runtime.Caller(1)
		log.Printf("[error] %s:%d %v", fn, line, err)
		b = true
	}
	return
}

// FancyHandleError logs the function name as well.
func FancyHandleError(err error) (b bool) {
	if err != nil {
		// notice that we're using 1, so it will actually log the where
		// the error happened, 0 = this function, we don't want that.
		pc, fn, line, _ := runtime.Caller(1)

		log.Printf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		b = true
	}
	return
}

// Excerpt Cut cuts string to limit
func Excerpt(text string, limit int, addDots bool) string {
	runes := []rune(text)
	if len(runes) >= limit {
		if addDots {
			return fmt.Sprintf("%s...", string(runes[:limit]))
		}
		return string(runes[:limit])
	}
	return text
}

// IsAuthenticated get login status of user
func IsAuthenticated(r *http.Request) bool {
	exists := app.Session.Exists(r.Context(), "user_id")
	return exists
}
