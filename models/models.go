package models

// TypeSupporting тип услуги поддрерживающая уборка
const TypeSupporting = 1

// TypeGeneral тип услуги генеральная уборка
const TypeGeneral = 2

// TypeAfterReconstruction тип услуги уборка после ремонта
const TypeAfterReconstruction = 3

// TypeWindows  тип услуги мытье окон
const TypeWindows = 4

// PageSupportingID  id страницы услуги поддрерживающая уборка
const PageSupportingID = 4

// PageGeneralID id страницы генеральная уборка
const PageGeneralID = 6

// PageAfterReconstructionID id страницы генеральная уборка
const PageAfterReconstructionID = 5

// PageWindowsID id страницы мойка окон
const PageWindowsID = 11

// FuncMap ...
// var FuncMap = template.FuncMap{
// 	"increment": Increment,
// 	"decrement": Decrement,
// }
