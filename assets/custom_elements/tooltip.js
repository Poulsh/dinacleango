class Tooltip extends HTMLElement {
    constructor(){
        super(); 
        
        console.log('constructor()')
   
        this._tooltipIcon;
        this._tooltipVisible = false;
        this._tooltipText = 'текст по умолчанию';
        this.attachShadow({mode: 'open'});

        // const template = document.querySelector('#tooltip-template');
        // this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.shadowRoot.innerHTML = `
        <style>
            div {
                position: absolute;
                top:1.5rem;
                left: 0.75rem;
                background-color: black;
                color: white;
                padding: .4em;
                z-index:10;
                box-shadow: 2px 2px 6px rgba(0,0,0,0.36);
                font-weight: normal;
            }
            :host {
                position: relative;
            }
            :host(.important){
                background: var(--color-primary, #eee);
                padding:.25rem;
            
            }
            :host-context(p) {
                font-weight:bold;
                color:red;
            }
            ::slotted(.highlight) {
                // padding:.25rem;
                // border-bottom: 5px dotted red;
            }
            .icon {
                background: black;
                color:white;
                padding:.25rem;
                text-align: center;
                border-radius: 5px;
            }
        </style>   
        <slot>Дефолтное значение слота</slot> 
        <span class="icon">(?)</span>
        `;
    }

    connectedCallback(){
        if(this.hasAttribute('text')){
            this._tooltipText = this.getAttribute('text');
        }
       
        this._tooltipIcon = this.shadowRoot.querySelector('span');
     
        this._tooltipIcon.addEventListener(
            'mouseenter',
            this._showTooltip.bind(this));
        this._tooltipIcon.addEventListener(
            'mouseleave', 
            this._hideTooltip.bind(this));
        // this.style.position = 'relative';
        this._render();
    }

    attributeChangedCallback(name, oldValue, newValue){
        if(oldValue === newValue){
            return;
        }
        if (name === 'text'){
            this._tooltipText = newValue;
        }
    }

    static get observedAttributes(){
        return ['text'];
    }

    disconnectedCallback(){
        this._tooltipIcon.removeEventListener('mouseenter', this._showTooltip);
        this._tooltipIcon.removeEventListener('mouseleave', this._hideTooltip);
    }

    _render() {
        let tooltipContainer = this.shadowRoot.querySelector('div');
        if(this._tooltipVisible){
            tooltipContainer = document.createElement('div');
            tooltipContainer.textContent = this._tooltipText;
            this.shadowRoot.appendChild(tooltipContainer);
        } else {
            if(tooltipContainer){
                this.shadowRoot.removeChild(tooltipContainer);
            } 
        }
    }

    _showTooltip(){
        this._tooltipVisible = true;
        this._render();
    }
    _hideTooltip() {
       this._tooltipVisible = false;
       this._render();
    }
}

customElements.define('sh-tooltip', Tooltip);